class LanguageCard {

    /**
    * @param data { object }
    */
    constructor(data) {
        this.__cardContainer = document.createElement("div")
        this.__cardContainer.classList.add("cardContainer")

        this.__data = data;
        this.__displayDescription = false

        this.generateCardHeader()
        this.generateCardBody()
        this.generateCardFooter()
    }

    getLanguageCardContainer() { return this.__cardContainer }

    generateCardHeader() {
        this.__headerContainer = document.createElement("div")
        this.__headerContainer.classList.add("headerContainer")
        this.__headerContainer.style.backgroundColor = this.__data.secondColor
        this.__headerContainer.style.borderBottom = this.__data.color + '15px solid'

        this.__titleHead = document.createElement("h1")
        this.__titleHead.innerText = this.__data.name.toUpperCase()
        this.__titleHead.classList.add("cardTitle");

        this.__headerContainer.appendChild(this.__titleHead)


        this.__cardContainer.appendChild(this.__headerContainer)
    }

    generateCardBody() {
        this.__bodyContainer = document.createElement("div")
        this.__bodyContainer.classList.add("bodyContainer")

        this.__imageContainer = document.createElement("div")
        this.__imageContainer.classList.add("imageContainer")

        const imageContainer = document.createElement("img")
        imageContainer.setAttribute("src", '/imgs/' + this.__data.logo)
        
        this.__button = document.createElement("button")
        this.__button.classList.add("buttonCard")
        this.__button.innerText = "Voir la description"
        this.__button.style.backgroundColor = this.__data.secondColor

        this.__button.addEventListener('click', () => {
            if (this.__displayDescription === false) {
                this.displayDescription()
                this.__button.style.backgroundColor = this.__data.secondColor
            } else {
                this.hideDescription()
                this.__button.style.backgroundColor = this.__data.secondColor
            }
        })
        
        this.__imageContainer.appendChild(imageContainer)
        this.__bodyContainer.appendChild(this.__imageContainer)
        this.__cardContainer.appendChild(this.__bodyContainer)
        this.__bodyContainer.appendChild(this.__button)
    }

    generateCardFooter() {
        this.__footerContainer = document.createElement("div")
        this.__footerContainer.classList.add("footerContainer")
        this.__footerContainer.style.backgroundColor = this.__data.secondColor
        this.__footerContainer.style.borderTop = this.__data.color + '15px solid'

        this.__cardContainer.appendChild(this.__footerContainer)
    }

    hideDescription(){
        // Supprime le container de la description
        this.__bodyContainer.removeChild(this.__bodyContainer.lastChild)

        // change les style du button
        this.__button.innerText = "Voir la description"
        this.__button.style.backgroundColor = null

        // change la state de l'affichage
        this.__displayDescription = false

        // Transition de retour de la div
        this.__cardContainer.classList.add('horizBack');
        this.__cardContainer.classList.remove('horizTranslate');
    }

    displayDescription(){
        // Affichage de la description
        this.__descriptionContainer = document.createElement("div")
        this.__descriptionContainer.classList.add("infosContainer")
        const infos = ["description"]

        infos.forEach((key) => {
            const infoContainer = document.createElement("p")
            infoContainer.innerText = key + " : " + this.__data[key]
            this.__descriptionContainer.appendChild(infoContainer)
        })

        this.__bodyContainer.appendChild(this.__descriptionContainer)

        // change l'état de l'affichage
        this.__displayDescription = true

        // changer le button
        this.__button.innerText = "Cacher la description"

        // fait la transition d'ouverture
        if(this.__cardContainer.classList.contains('horizBack')){
            // si déjà ouvert on supprimer la class de retour
            this.__cardContainer.classList.remove('horizBack');
        }
        this.__cardContainer.classList.add('horizTranslate');
    }
}