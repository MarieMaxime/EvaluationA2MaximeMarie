# Prerequis

Pour lancer le projet il faut un plugins VSCode:
	ritwickdey.liveserver

# Remarque 

Je n'ai pas réussi à faire fonctionner les animations, ni mettre en place les triangles sur les frameworks.
J'ai aussi un petit soucis sur la hauteur des cards

# Evaluation de fin d’année A2

## A. Énoncer
Le but de l’exercice est de réaliser l’intégration de la template. Le but est de lire le
fichier “data.json” et d'afficher le contenu. Le but est de réaliser le moins d’html possible et
que tout se fasse en JS. Le but est ici de tester vos compétences JS. L’HTML CSS est ici
vraiment secondaire. Si le CSS n’est pas respecté à la lettre ce n’est pas très grave
concentrez-vous sur le JS
La notation expliquée plus bas l’indique un petit peu, mais faites au départ
extrêmement simple avec des boucles et des fonctions puis factorisez en objet voire en
classe JS. N’oubliez pas de commenter votre code ! Très important ici.
lien de la template :

	https://www.figma.com/proto/XUtUNmLfaPhBofN1L25sPC/Evaluation?page-id=0%3A1&node-id=80%3A251&viewport=1615%2C-159%2C0.5244232416152954&scaling=min-zoom

Bon courage vous avez la journée !