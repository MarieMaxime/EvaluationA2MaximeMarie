// get data from the json file
fetch('src/data.json')
    .then(resp => resp.json())
    .then(data => render(data))

    // rendering elements
/**
 * 
 * @param {object} data 
 */
async function render(data) {
    await generateLanguage(data)
    await generateFrameworksFront(data)
    await generateFrameworksBack(data)
    await generateframeworksCrossPlateform(data)
}

// generate titles
/**
 * 
 * @param {HTMLElement} content 
 * @param {string} container 
 */
async function titles(content, container) {
    let div = document.createElement("div")
    div.style.cssText = "width: 100%;";
    container.appendChild(div)
    this.title = document.createElement("h2")
    this.title.classList.add('titles')
    this.title.innerText = content
    div.appendChild(this.title)
}

// get container by ID
const mainContainer = document.getElementById('mainContainer')
const languagesContainer = document.getElementById("languagesContainer")
const frameworksFrontContainer = document.getElementById("frameworksFrontContainer")
const frameworksBackContainer = document.getElementById("frameworksBackContainer")
const frameworksCrossPlateformContainer = document.getElementById("frameworksCrossPlateformContainer")

// declarations of generation funtions
async function generateLanguage(data) {
    // call titles function
    await titles('1. Les languages du Web', languagesContainer)
    data.languages.forEach(function (language) {
        const languageCard = new LanguageCard(language)
        languagesContainer.appendChild(languageCard.getLanguageCardContainer());
    })
}

async function generateFrameworksFront(data) {
    await titles('2. Les frameworks front', frameworksFrontContainer)
    data.frameworks.forEach(function (framework) {
        const index = framework.tags.findIndex(tag => tag === 'front')
        if (index !== -1) {
            const frameworksFrontCard = new FrameworksCard(framework)
            frameworksFrontContainer.appendChild(frameworksFrontCard.getFrameworkCardContainer());
        }
    })
}

async function generateFrameworksBack(data) {
    await titles('3. Les frameworks back', frameworksBackContainer)
    data.frameworks.forEach(function (framework) {
        const index = framework.tags.findIndex(tag => tag === 'back')
        if (index !== -1) {
            const frameworksBackCard = new FrameworksCard(framework)
            frameworksBackContainer.appendChild(frameworksBackCard.getFrameworkCardContainer());
        }
    })
}

async function generateframeworksCrossPlateform(data) {
    await titles('4. Les frameworks cross plateform', frameworksBackContainer)
    data.frameworks.forEach(function (framework) {
        const index = framework.tags.findIndex(tag => tag === 'crossplateform')
        if (index !== -1) {
            const frameworksCrossPlateformCard = new FrameworksCard(framework)
            frameworksCrossPlateformContainer.appendChild(frameworksCrossPlateformCard.getFrameworkCardContainer());
        }
    })
}